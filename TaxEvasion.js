// Variables
var minimalQuantile = 25;
var maximalQuantile = 75;

var resourceAmount = 16;
var tradeTick = 1000 * 60;


// Init
InitializeStockMarket();


// Functions
function InitializeStockMarket() {
    if (typeof InsugarTrading === 'undefined') {
        Game.LoadMod('https://staticvariablejames.github.io/InsugarTrading/InsugarTrading.js');
    }
    clearInterval(StockMarketManager);
}

var StockMarketManager = setInterval(() => {
    if (typeof InsugarTrading !== 'undefined') {
        StockMarketUpdateQuantiles();
        StockMarketTradeTick();
    } else {
        console.log("Waiting for Insugar to be loaded");
    }
}, tradeTick);

function StockMarketUpdateQuantiles() {
    let lowestQuantile = 100;
    let highestQuantile = 0;

    InsugarTrading.settings.quantilesToDisplay.forEach(quantilePercentage => {
        let quantile = quantilePercentage * 100;
        if (quantile < lowestQuantile) {
            lowestQuantile = quantile;
        } else if (quantile > highestQuantile) {
            highestQuantile = quantile;
        }
    });

    if (minimalQuantile != lowestQuantile || maximalQuantile != highestQuantile) {
        minimalQuantile = lowestQuantile;
        maximalQuantile = highestQuantile;
        console.log(`Quantiles set to new min/max: ${minimalQuantile} / ${maximalQuantile}`);
    }
}

function StockMarketTradeTick() {
    for (let resource = 0; resource < resourceAmount; resource++) {
        let quantileText = document.querySelector(`#quantile-${resource}`);
        if (quantileText != null) {

            let stockText = document.querySelector(`#bankGood-${resource}-stock`).innerText;
            let stockAmount = parseFloat(stockText.replace("\,", ""));
            let stockMaxText = document.querySelector(`#bankGood-${resource}-stockMax`).innerText;
            let stockMaxAmount = parseFloat(stockMaxText.replace("\,", "").replace("\/", ""));

            let quantileAmount = parseFloat(quantileText.innerText);
            if (quantileAmount < minimalQuantile && stockAmount < stockMaxAmount) {
                console.log(`purchasing resource: ${resource}, at quantile: ${quantileAmount}`);
                BuyAllResource(resource);

            } else if (quantileAmount > maximalQuantile && stockAmount > 0) {
                console.log(`selling resource: ${resource}, at quantile: ${quantileAmount}`);
                SellAllResource(resource);
            }

        } else {
            console.log("Waiting for Insugar to be loaded");
            break;
        }
    }
};

function BuyAllResource(resource) {
    document.querySelector("#bankGood-" + resource + "_Max").click();
};

function SellAllResource(resource) {
    document.querySelector("#bankGood-" + resource + "_-All").click();
};
